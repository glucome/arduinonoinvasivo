#include <MsTimer2.h>
#include <Wire.h>
#include <SoftwareSerial.h>


SoftwareSerial btSerial(12, 11); // 12 RX, 11 TX.

const int BATERIA = A6;
const int sensorPin9402nm = A5;
const int sensorPin940nm = A4;

const int multiplex9402nm = 3;
const int multiplex940nm = 4;
const int LED_BATERIA_BAJA = 2;
const int LED_CALCULANDO = 8;
const int LED_ENCENDIDO = 9;

const float maxVoltage = 5.0;
const float sensorAnalogMax = 1023;

int btCode;
int sensorValue9402nm;
int sensorValue940nm;
int measurementCounter;
int averageCounter;

float value9402nm;
float value940nm;
float averageValue9402nm;
float averageValue940nm;
float finalAverageValue9402nm;
float finalAverageValue940nm;

boolean firstMeasure;
boolean hasToContinueMeasuring;
boolean volatile finishMeasure;

void setup() {

  pinMode(multiplex9402nm, OUTPUT);
  pinMode(multiplex940nm, OUTPUT);

  digitalWrite(multiplex9402nm, LOW);
  digitalWrite(multiplex940nm, LOW);

  pinMode(LED_BATERIA_BAJA, OUTPUT);
  pinMode(LED_CALCULANDO, OUTPUT);
  pinMode(LED_ENCENDIDO, OUTPUT);

  averageValue9402nm = 0;
  averageValue940nm = 0;

  measurementCounter = 0;
  averageCounter = 0;
  firstMeasure = true;
  hasToContinueMeasuring = true;
  MsTimer2::set(25, checkMeasurements);
  MsTimer2::start();

  Serial.begin(9600);
  btSerial.begin(9600);
}

void loop() {

  digitalWrite(LED_ENCENDIDO, LOW);
  digitalWrite(LED_CALCULANDO, HIGH);
  checkBatteryLevel();



  if (btSerial.available() > 0) {
    btSerial.flush();
    finishMeasure = false;
    checkBatteryLevel();
    btCode = btSerial.read();

    if (btCode == 1 || btCode == 49) {
      //btSerial.println("Conecte");
      interrupts();
      while (!finishMeasure);
        
      
    }
  }

}

void checkMeasurements() {

  if (!finishMeasure) {
    if (measurementCounter < 200) {
      measureFinger();
      measurementCounter = measurementCounter + 1;
    } else {

      //Descarto la primer medicion
      if (!firstMeasure) {

        averageValue9402nm /= 100.0;
        averageValue940nm /= 100.0;

        if (isAcceptableDeviation()) {
          hasToContinueMeasuring = false;
        }

        //Inicializo a finalAverageValue para hacer el promedio
        if (averageCounter == 0) {
          finalAverageValue9402nm = averageValue9402nm;
          finalAverageValue940nm = averageValue940nm;
        }

        if (!hasToContinueMeasuring || averageCounter > 5) {

          digitalWrite(LED_CALCULANDO, HIGH);

          float finalVoltage = (finalAverageValue9402nm + finalAverageValue940nm) / 2;
          float glucoseLevel = (-23, 1) * finalVoltage + 166;

          String glucoseLevelString = String(glucoseLevel, 4);
          btSerial.println(glucoseLevelString);

          finishMeasure = true;
          firstMeasure = true;
          hasToContinueMeasuring = true;
          averageCounter = 0;
          finalAverageValue9402nm = 0;
          finalAverageValue940nm = 0;

        } else {
          averageCounter++;

          finalAverageValue9402nm = (finalAverageValue9402nm + averageValue9402nm) / 2;
          finalAverageValue940nm = (finalAverageValue940nm + averageValue940nm) / 2;

          measurementCounter = 0;
          averageValue9402nm = 0;
          averageValue940nm = 0;

          digitalWrite(multiplex9402nm, LOW);
          digitalWrite(multiplex940nm, LOW);
        }


      } else {
        digitalWrite(LED_CALCULANDO, LOW);
        firstMeasure = false;
        measurementCounter = 0;
        averageValue9402nm = 0;
        averageValue940nm = 0;
      }

    }

  }

}

void measureFinger() {

  if (measurementCounter >= 0 && measurementCounter < 100) {
    digitalWrite(multiplex9402nm, LOW);
    digitalWrite(multiplex940nm, HIGH);
    sensorValue940nm = analogRead(sensorPin940nm);
    value940nm = conversion(sensorValue940nm);
    averageValue940nm += value940nm;
  } else if (measurementCounter >= 100 && measurementCounter < 200) {
    digitalWrite(multiplex940nm, LOW);
    digitalWrite(multiplex9402nm, HIGH);
    sensorValue9402nm = analogRead(sensorPin9402nm);
    value9402nm = conversion(sensorValue9402nm);
    averageValue9402nm += value9402nm;
  }

}

boolean isAcceptableDeviation() {
  float absoluteDeviation9402nm;
  float absoluteDeviation940nm;

  float percentageDeviation9402nm;
  float percentageDeviation940nm;

  if (finalAverageValue940nm != 0) {

    absoluteDeviation9402nm = finalAverageValue9402nm - averageValue9402nm;
    absoluteDeviation940nm = finalAverageValue940nm - averageValue940nm;

    Serial.print("finalAverageValue9402nm en deviation : ");
    Serial.println(finalAverageValue9402nm);
    Serial.print("finalAverageValue940nm en deviation : ");
    Serial.println(finalAverageValue940nm);

    Serial.print("averageValue9402nm en deviation : ");
    Serial.println(averageValue9402nm);
    Serial.print("averageValue940nm en deviation : ");
    Serial.println(averageValue940nm);

    percentageDeviation9402nm =  abs(absoluteDeviation9402nm) * 100 / finalAverageValue9402nm;
    percentageDeviation940nm =  abs(absoluteDeviation940nm) * 100 / finalAverageValue940nm;

    Serial.print("Desviacion porcentual de 9402nm: ");
    Serial.println(percentageDeviation9402nm);
    Serial.print("Desviacion porcentual de 940nm: ");
    Serial.println(percentageDeviation940nm);
    Serial.println("");

    return percentageDeviation940nm < 5 && percentageDeviation9402nm < 5;

  } else {
    return false;
  }

}

float conversion(float pin) {
  return pin * maxVoltage / sensorAnalogMax;
}

void checkBatteryLevel() {
  if (conversion(analogRead(BATERIA)) < 2.9) {
    digitalWrite(LED_BATERIA_BAJA, LOW);
  } else {
    digitalWrite(LED_BATERIA_BAJA, HIGH);
  }
}
